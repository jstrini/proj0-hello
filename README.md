# Proj0-Hello
Prints a user defined message in the terminal.
## Getting Started
- Clone this repository on to your machine.
- Copy the file credentials-skel.ini then rename it to credentials.ini.
- Fill out credentials.ini with your name, repo url and desired message.
- Move credentials.ini into the hello/ directory.
- Execute `make run` in the project's main directory to test.
## Authors

* Michael Young 
* Joe Strini
* contact me at: jstrini@uoregon.edu
